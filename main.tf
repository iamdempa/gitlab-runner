# provider "aws" {
#   region  = "us-west-1"
# }
resource "aws_s3_bucket" "banuka-terraform-state" {
  bucket = "banuka-terraform-state"
  tags = {
    Name = "banuka-terraform-state"
  }
}
 
resource "aws_dynamodb_table" "banuka-terraform-state" {
  name = "banuka-dynamodb-terraform-state"
  read_capacity = 20
  write_capacity = 20
  hash_key = "LockID"
 
  attribute {
      name = "LockID"
      type = "S"
  }
}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "banuka-terraform-state"
    key            = "terraform.tfstate"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "banuka-terraform-state"
    encrypt        = true
  }
}


# create the main vpc
resource "aws_vpc" "kubernetes_main_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "kubernetes_main_vpc"
  }
}


# # create public subnect
# resource "aws_subnet" "kubernetes_public_subnet" {
#   vpc_id = "${aws_vpc.kubernetes_main_vpc.id}"
#   cidr_block = "10.0.0.0/17"
#   availability_zone = "us-east-1a"

#   tags = {
#     Name = "kubernetes_public_subnet"
#   }
# }


# # create a private subnet
# resource "aws_subnet" "kubernetes_private_subnet" {
#   vpc_id = "${aws_vpc.kubernetes_main_vpc.id}"
#   cidr_block = "10.0.128.0/17"
#   availability_zone = "us-east-1b"

#   tags = {
#     Name = "kubernetes_private_subnet"
#   }
# }

# # route tables

# # #internet gateway
# # resource "aws_internet_gateway" "kubernetes-ig2" {
# #   vpc_id = "${aws_vpc.kubernetes_main_vpc.id}"

# #   tags = {
# #     Name = "kubernetes-ig2"
# #   }
# # }